import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import RouteLayout from './Route';
import './App.scss';

function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
    <RouteLayout />
  </Router>
  );
} 

export default App;
