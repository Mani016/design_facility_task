import React from 'react';
import { BrowserRouter as Router, Switch, Route,Redirect } from 'react-router-dom';
import Test from './components/Test';
import TestEnd from './components/TestEnd';
function RouteLayout() {
  return (
    <Router >
      <Switch>
      <Redirect from='/' to='/test' exact />
        <Route path="/test" component={Test} />
        <Route to='/test-end' component={TestEnd} />
      </Switch>
    </Router>  
  );
}

export default RouteLayout;
