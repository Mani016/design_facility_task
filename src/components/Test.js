import React from 'react';
import {
    Col,
    Button,
    Alert,
    Tab,
    Tabs,
    Row
} from 'react-bootstrap';
import axios from 'axios';
const Test = () => {
    const [examData, setExamData] = React.useState();
    const [examTitle, setExamTitle] = React.useState('');
    const [sections, setSections] = React.useState([]);
    const [questions, setQuestions] = React.useState([]);
    const [showWarning, setShowWarning] = React.useState(false); 
    const [showSuccess, setShoWSuccess] = React.useState(false);
    const [currentSectionIndex, setCurrentSectionIndex] = React.useState(0);
    const [currentQuestionIndex, setCurrentQuestionIndex] = React.useState(0);
    React.useEffect(() => {
        function getData() {
            axios.get(`http://5.181.217.46/DesignFacility/useGETMethodForTheResponse/manisha`)
            .then(res => {
                setExamData(res['data']['exam']);
            })
        }

        getData();
    }, []);

    React.useEffect(() => {
        if (examData) {
            setExamTitle(examData['examTitle']);
            setSections(examData['sections']);
            countdown(examData['examDurationInMinutes']);
        }
        function countdown(minutes) {
            var seconds = 60;
            var mins = minutes;
            function tick() {
                var counter = document.getElementById("counter");
                var current_minutes = mins - 1
                seconds--;
                counter.innerHTML = current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds);
                if (seconds > 0) {
                    setTimeout(tick, 1000);
                } else {
                    if (mins > 1) {
                        countdown(mins - 1);
                    }
                }
                if (current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds) === 0 + ':' + "00") {
                    window.location = '/test-end';
                }
                if (current_minutes.toString() + ":" + (seconds < 10 ? "0" : "") + String(seconds) === 1 + ':' + "00") {
                    setShowWarning(true)
                }
            }
            tick();
        }
    }, [examData]);
    function SelectQuestionIndex(s){
        if(currentQuestionIndex < questions['Section'+(currentSectionIndex+1)].length-1){
            setCurrentQuestionIndex(s+1)
            console.log(currentQuestionIndex)
        }
        else{
            setShoWSuccess(true);
            setCurrentQuestionIndex(0)
        }
    };
    React.useEffect(()=>{
        setQuestions(sections[currentSectionIndex]);
    },[sections, currentSectionIndex])
 function SubmitExam(){
     window.location = "/test-end";
 }

    return (
        <>
            { examData !== 'undefined' ?
                <div className="p-3">
                    <Alert show={showWarning} variant="warning">
                        <Alert.Heading>Less Than a Minute Remaining!</Alert.Heading>
                        <p>
                            You have less than a minute remaining be quick!!!!!!!
        </p>
                        <hr />
                        <div className="d-flex justify-content-end">
                            <Button onClick={() => setShowWarning(false)} variant="outline-warning">
                                Close me y'all!
          </Button>
                        </div>
                    </Alert>
                    <Alert show={showSuccess} variant="success">
                        <Alert.Heading>Section Completed </Alert.Heading>
                        <p>
                            Please Click on the Next Section Tab to start another section
                         </p>
                        <hr />
                        <div className="d-flex justify-content-end">
                            <Button onClick={() => setShoWSuccess(false)} variant="outline-sucess">
                                Close me y'all!
          </Button>
                        </div>
                    </Alert>
                    <Col lg={10} md={10} xs={10}>
                        <div className="d-flex justify-content-between">
                            <div className="d-flex">
                                <div className="title">
                                    Design Facility
           </div>
                                <div className="dot" />
                                <div className="exam-title">
                                    {examTitle}
                                </div>
                            </div>
                            <div className="time-left-container" >
                                <div className="time-left"> <div>Time left = <span id="counter"></span></div></div>
                            </div>
                        </div>
                        <Tabs
                            id="noanim-tab-example"
                            defaultActiveKey='Section1'
                            onSelect={(k) =>{setCurrentSectionIndex(k.substr(-1)-1)}}
                        >
                            {sections.map((text, index) => {
                                return (
                                    <Tab eventKey={Object.keys(text)} title={Object.keys(text)} key={index} >
                                        {text['Section' + (currentSectionIndex + 1)] ?
                                            <div className="padding-bottom">
                                            <div className="question-number">QUESTION NUMBER {currentQuestionIndex + 1}</div>
                                            <div className="question-text">{text['Section' + (currentSectionIndex + 1)][currentQuestionIndex]['quesText']}</div>
                                            <Row className="margin-top">
                                                <Col lg={12} md={12} xs={12}>
                                                    <div className="d-flex">
                                                        <div className="option-mark">
                                                            <div className="option-mark-text">A</div>
                                                        </div>
                                                        <div className="option-text-container w-100">
                                                            <div className="option-text">
                                                            {text['Section' + (currentSectionIndex + 1)][currentQuestionIndex]['option1']}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="d-flex mt-3">
                                                        <div className="option-mark">
                                                            <div className="option-mark-text">B</div>
                                                        </div>
                                                        <div className="option-text-container w-100">
                                                            <div className="option-text">
                                                           { text['Section' + (currentSectionIndex + 1)][currentQuestionIndex]['option2']}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="d-flex mt-3">
                                                        <div className="option-mark">
                                                            <div className="option-mark-text">C</div>
                                                        </div>
                                                        <div className="option-text-container w-100">
                                                            <div className="option-text">
                                                                {text['Section' + (currentSectionIndex + 1)][currentQuestionIndex]['option3']}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="d-flex mt-3">
                                                        <div className="option-mark">
                                                            <div className="option-mark-text">D</div>
                                                        </div>
                                                        <div className="option-text-container w-100">
                                                            <div className="option-text">
                                                            {text['Section' + (currentSectionIndex + 1)][currentQuestionIndex]['option4']}
                                                            </div>
                                                        </div>
                                                    </div>
        
                                                </Col>
        
                                            </Row>
                                        </div>
                                            : <></>}
                                    </Tab>
                                )
                            })
                            }


                        </Tabs>
                        <div className="footer-container">
                            <div className="d-flex justify-content-between w-100 p-2">
                                <div className="d-flex">
                                    <Button className="mr-3 action-buttons">
                                        <div className="button-text">Mark For Review & Next</div>
                                    </Button>
                                    <Button className="action-buttons">
                                        <div className="button-text"> Clear Response</div>
                                    </Button>
                                </div>
                                <div className="d-flex">
                                    <Button className="action-button-save" onClick={(k)=>SelectQuestionIndex(currentQuestionIndex)}>
                                        <div className="action-button-save-text">Save & Next</div>
                                    </Button>
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col lg={2} md={2} xs={2}>
                        <div className="d-flex p-4">
                            <div className="candidate-image" />
                            <div className="candidate-message">Best Of Luck !</div>
                        </div>
                        <div className="d-flex">
                            <div className="question-marks-not-visited">
                                <div className="text-white question-marks">2</div>
                            </div>
                            <div className="question-marks-text">
                                Question Not Visited
                            </div>
                        </div>
                        <div className="d-flex mt-2">
                            <div className="question-marks-not-answered">
                                <div className="text-white question-marks">2</div>
                            </div>
                            <div className="question-marks-text">
                                Question Not Answered
          </div>
                        </div>
                        <div className="d-flex mt-2">
                            <div className="question-marks-answered">
                                <div className="text-white question-marks">2</div>
                            </div>
                            <div className="question-marks-text">
                                Question Answered
          </div>
                        </div>
                        <div className="d-flex mt-2">
                            <div className="question-marks-marked">
                                <div className="text-white question-marks">4</div>
                            </div>
                            <div className="question-marks-text">
                                Questions Marked
          </div>
                        </div>
                        <div className="d-flex mt-2">
                            <div className="question-marks-review">
                                <div className="text-white question-marks">5</div>
                            </div>
                            <div className="question-marks-text">
                                Question Marked & Marked for Review
          </div>
                        </div>
                        <div className="section-title">
                            <div className="section-title-text mt-3">{sections.length === 0 ? '' : Object.keys(sections[currentSectionIndex])[0] }</div>
                        </div>
                        <div className="question">Choose  a question:</div>
                        <div className="d-flex">
                            {sections[currentSectionIndex] ? 
                            sections[currentSectionIndex]['Section'+(currentSectionIndex+1)].map((text,index)=>{
                                console.log(currentQuestionIndex,index)
                                return(
                                <Button className={currentQuestionIndex+1 === (index+1) ? "option-mark ml-4 option-mark-not-attempted" : 'option-mark ml-4 '}><div className="option-mark-text1">{index+1}</div></Button>
                                )
                            })
                                :<></>
                             }
                        </div>
                        <div>
                            <Button className="action-button-submit" onClick={()=>SubmitExam()}>
                                <div className="action-button-submit-text" >Submit Exam</div>
                            </Button>
                        </div>
                    </Col>
                </div>
                : <></>}</>
    );
}

export default Test;
